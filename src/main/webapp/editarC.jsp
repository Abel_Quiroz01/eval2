<%-- 
    Document   : editarC.jsp
    Created on : Apr 24, 2021, 5:41:11 PM
    Author     : Abel
--%>

<%@page import="root.Consultamedica1.entity.Consultamedica1"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Consultamedica1 solicitud =(Consultamedica1) request.getAttribute("solicitud");
%>    
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Consulta Medica Fonasa</h1>
        <form name="form" action="Consultacontroller" method="POST">
            <input type="text" placeholder="rut" disabled="true" name="rut" id="rut" value="<%= solicitud.getRut()%>">
            <input type="text" placeholder="letra Fonasa" name="letrafonasa" id="letrafonasa" value="<%= solicitud.getLetrafonasa()%>">
            <input type="text" placeholder="nombre" name="nombre" id="nombre" value="<%= solicitud.getNombre()%>">
            
            
            <button type="submit" name="action" value="editarx" class="btn btn-success">editar consulta</button>
            
            
        </form>    
    </body>
</html>
