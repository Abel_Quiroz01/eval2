<%-- 
    Document   : index.jsp
    Created on : Apr 24, 2021, 5:41:11 PM
    Author     : Abel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Consulta Medica Fonasa</h1>
        <h1>URL GET : https://abelquirozeva3.herokuapp.com/api/consulta</h1>
        <h1>URL POST : https://abelquirozeva3.herokuapp.com/api/consulta</h1>
        <h1>URL DELETE : https://abelquirozeva3.herokuapp.com/api/consulta/{iddelete}</h1>
        <h1>URL PUT : https://abelquirozeva3.herokuapp.com/api/consulta</h1>
    </body>
</html>
