/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.Consultamedica1.dao.Consultamedica1JpaController;
import root.Consultamedica1.dao.exceptions.NonexistentEntityException;
import root.Consultamedica1.entity.Consultamedica1;

/**
 *
 * @author Abel
 */
@WebServlet(name = "Consultacontroller", urlPatterns = {"/Consultacontroller"})
public class Consultacontroller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Consultacontroller</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Consultacontroller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            String action=request.getParameter("action");
            
                if(action.equals("newconsulta")){
                     
        try {
            String rut=request.getParameter("rut");
            String nombre=request.getParameter("nombre");
            String letrafonasa=request.getParameter("letrafonasa");
            
            Consultamedica1  consultamedica=new Consultamedica1();
            consultamedica.setRut(rut);
            consultamedica.setNombre(nombre);
            consultamedica.setLetrafonasa(letrafonasa);
            
            Consultamedica1JpaController dao =new Consultamedica1JpaController();
            dao.create(consultamedica);
            
            
            
        } catch (Exception ex) {
            Logger.getLogger(Consultacontroller.class.getName()).log(Level.SEVERE, null, ex);
        }
         processRequest(request, response);
                }
               if(action.equals("seelista")){
                Consultamedica1JpaController dao =new Consultamedica1JpaController();
                List<Consultamedica1> lista=dao.findConsultamedica1Entities();
                request.setAttribute("listaConsulta", lista);
                request.getRequestDispatcher("list_con.jsp").forward(request, response);
                
                
                
                }
                if(action.equals("eliminar")){
                try {
                    String rut = request.getParameter("seleccion1");
                    Consultamedica1JpaController dao =new Consultamedica1JpaController();
                    dao.destroy(rut);
                }
                 catch (NonexistentEntityException ex) {
                    Logger.getLogger(Consultacontroller.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                
               
                }
                if(action.equals("editar")){                
                    String rut = request.getParameter("seleccion1");
                    Consultamedica1JpaController dao =new Consultamedica1JpaController();
                    Consultamedica1 inscripcion = dao.findConsultamedica1(rut);
                    request.setAttribute("solicitud", inscripcion);
                    request.getRequestDispatcher("editarC.jsp").forward(request, response);
                    
               
                
              
                }
                if(action.equals("editarx")){                
                try {
                    String rut=request.getParameter("rut");
                    String nombre=request.getParameter("nombre");
                    String letrafonasa=request.getParameter("letrafonasa");
                    
                    Consultamedica1  consultamedica=new Consultamedica1();
                    consultamedica.setRut(rut);
                    consultamedica.setNombre(nombre);
                    consultamedica.setLetrafonasa(letrafonasa);
                    
                    Consultamedica1JpaController dao =new Consultamedica1JpaController();
                    dao.edit(consultamedica);
                } catch (Exception ex) {
                    Logger.getLogger(Consultacontroller.class.getName()).log(Level.SEVERE, null, ex);
                }
            
               
                
              
                }
                processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
