/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.Consultamedica1.dao.Consultamedica1JpaController;
import root.Consultamedica1.dao.exceptions.NonexistentEntityException;
import root.Consultamedica1.entity.Consultamedica1;

/**
 *
 * @author Abel
 */
@Path ("consulta")
public class Cmedicas1 extends HttpServlet {
    
   @GET
   @Produces(MediaType.APPLICATION_JSON)
   public Response listarConsultas(){
       
    /*Consultamedica1 tr1=new Consultamedica1();
    tr1.setLetrafonasa("a");
    tr1.setNombre("abel");
    tr1.setRut("48");
    Consultamedica1 tr2=new Consultamedica1();
    tr2.setLetrafonasa("c");
    tr2.setNombre("abelx");
    tr2.setRut("18");
    
    List<Consultamedica1> Lista=new ArrayList<Consultamedica1>();
    
    Lista.add(tr1);
    Lista.add(tr2);*/
    
    Consultamedica1JpaController dao= new Consultamedica1JpaController();
    
    List<Consultamedica1> Lista= dao.findConsultamedica1Entities();
    
    return Response.ok(200).entity(Lista).build();
       
   }
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response add(Consultamedica1 consultamedica){
    
       try {
           Consultamedica1JpaController dao= new Consultamedica1JpaController();
           dao.create(consultamedica);
       } catch (Exception ex) {
           Logger.getLogger(Cmedicas1.class.getName()).log(Level.SEVERE, null, ex);
       }
        
       return Response.ok(200).entity(consultamedica).build();
    }
    @DELETE
    @Path("/{iddelete}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("iddelete") String iddelete){
    
       try {
           Consultamedica1JpaController dao= new Consultamedica1JpaController();
           dao.destroy(iddelete);
       } catch (NonexistentEntityException ex) {
           Logger.getLogger(Cmedicas1.class.getName()).log(Level.SEVERE, null, ex);
       }
      return Response.ok("cliente eliminado") .build();
    }
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(Consultamedica1 consultamedica){
       try {
           Consultamedica1JpaController dao= new Consultamedica1JpaController();
           dao.edit(consultamedica);
       } catch (Exception ex) {
           Logger.getLogger(Cmedicas1.class.getName()).log(Level.SEVERE, null, ex);
       }
       return Response.ok(200).entity(consultamedica).build();
    }

}