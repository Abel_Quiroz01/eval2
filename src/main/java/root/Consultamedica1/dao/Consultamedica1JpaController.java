/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.Consultamedica1.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.Consultamedica1.dao.exceptions.NonexistentEntityException;
import root.Consultamedica1.dao.exceptions.PreexistingEntityException;
import root.Consultamedica1.entity.Consultamedica1;

/**
 *
 * @author Abel
 */
public class Consultamedica1JpaController implements Serializable {

    public Consultamedica1JpaController() {
        
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

  

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Consultamedica1 consultamedica1) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(consultamedica1);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findConsultamedica1(consultamedica1.getRut()) != null) {
                throw new PreexistingEntityException("Consultamedica1 " + consultamedica1 + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Consultamedica1 consultamedica1) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            consultamedica1 = em.merge(consultamedica1);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = consultamedica1.getRut();
                if (findConsultamedica1(id) == null) {
                    throw new NonexistentEntityException("The consultamedica1 with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Consultamedica1 consultamedica1;
            try {
                consultamedica1 = em.getReference(Consultamedica1.class, id);
                consultamedica1.getRut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The consultamedica1 with id " + id + " no longer exists.", enfe);
            }
            em.remove(consultamedica1);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Consultamedica1> findConsultamedica1Entities() {
        return findConsultamedica1Entities(true, -1, -1);
    }

    public List<Consultamedica1> findConsultamedica1Entities(int maxResults, int firstResult) {
        return findConsultamedica1Entities(false, maxResults, firstResult);
    }

    private List<Consultamedica1> findConsultamedica1Entities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Consultamedica1.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Consultamedica1 findConsultamedica1(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Consultamedica1.class, id);
        } finally {
            em.close();
        }
    }

    public int getConsultamedica1Count() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Consultamedica1> rt = cq.from(Consultamedica1.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
